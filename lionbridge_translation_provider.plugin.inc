<?php

/**
 * @file
 * Controller class for Lionbridge translator plugin.
 */

/**
 * Lionbridge translation plugin controller.
 */
class TMGMTLionbridgeTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {
    switch ($job->state) {
      case 0:
        try {
          $this->submitJob($job);
          if (!$job->isRejected()) {
            $job->submitted(t('Job has been submitted.'));
          }
        }
        catch (TMGMTException $e) {
          watchdog_exception('lionbridge_translation_provider', $e);
          $job->rejected('Job has been rejected with following error: @error',
            array('@error' => $e->getMessage()), 'error');
        }
        break;

      case 1:
        try {
          $this->fetchJob($job);
        }
        catch (TMGMTException $e) {
          watchdog_exception('lionbridge_translation_provider', $e);
          $job->addMessage('Translation could not be completed: @error',
            array('@error' => $e->getMessage()), 'error');
        }
        break;
    }

  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('access_key_id') && $translator->getSetting('access_key')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * TMGMTDefaultTranslatorPluginController::getSupportedRemoteLanguages().
   */
  public function getSupportedRemoteLanguages(TMGMTTranslator $translator) {
    if ($translator->isAvailable($translator) == FALSE) {
      return parent::getSupportedRemoteLanguages($translator);
    }

    if (!empty($this->supportedRemoteLanguages)) {
      return $this->supportedRemoteLanguages;
    }

    try {
      $api_client = new TMGMTLionbridgeConnector($translator);
      $languages = $api_client->listLocales();
      if (!isset($languages['Error']) && !isset($languages['head']) && !isset($languages['body'])) {
        foreach ($languages['Locale'] as $language) {
          $this->supportedRemoteLanguages[$language['Code']] = $language['Code'];
        }
      }
    }
    catch (TMGMTException $e) {
      watchdog_exception('lionbridge_translation_provider', $e);
      drupal_set_message($e->getMessage(), 'error');
    }

    return $this->supportedRemoteLanguages;
  }

  /**
   * Submits a job to Lionbridge translation service.
   *
   * This does the work of actually submitting the translation job to the
   * Lionbridge translation service. There are 3 major parts to submitting a
   * translations job. Each part has been broken out into an individual
   * function to make the code more readable.
   *
   * 1. Create a JSON encoded file that contains all the translatable content
   *    and send it to Lionbridge. If this is successful, an array containing
   *    the details of the uploaded file is returned.
   *
   * 2. Create a project using the settings from the job settings page and the
   *    AssetID of the array returned from the successful file upload. If there
   *    were more than one file uploaded, multiple file asset ID's could be
   *    used in a project, however, in this model there is only 1 asset ID for
   *    a given project.
   *
   * 3. Generate a quote for the translation job using the Project ID from the
   *    array returned from a successful project creation request.
   *
   * @param \TMGMTJob $job
   *   The job object.
   */
  public function submitJob(TMGMTJob $job) {
    $translator = $job->getTranslator();
    $api_client = new TMGMTLionbridgeConnector($translator);
    $data_flat = array_filter(tmgmt_flatten_data($job->getData()), '_tmgmt_filter_data');
    $items = $job->getItems();
    $file_assets = $product_assets = array();
    $source_language = $translator->mapToRemoteLanguage($job->source_language);
    $target_language = $translator->mapToRemoteLanguage($job->target_language);
    $target_languages = array($target_language);

    // Create a logical project title.
    $project_title = $job->settings['project_title'] . ' to ' . $target_language;

    // Set the service id.
    $service_id = $translator->getSetting('service');

    // This is the name of the JSON file that will be sent to Lionbridge.
    $file_name = _lionbridge_translation_provider_generate_file_name($job->label, $source_language, $target_language);

    if ($translator->getSetting('notification_url')) {
      $project_complete_url = $translator->getSetting('notification_url');
    }
    else {
      $project_complete_url = url('lionbridge_translation_provider_project_complete_callback', array('absolute' => TRUE));
    }

    /*
     * Create an array of job item metadata. This is necessary because there
     * is nothing in the tmgmt_remote table yet to lookup these job items and
     * we need a way to refer to them later.
     */
    $translation_keys = $translatable_content = array();

    foreach ($data_flat as $key => $value) {
      list($tjiid, $data_item_key) = explode('][', $key, 2);

      // Preserve the original data.
      $translation_keys[] = array(
        'tjiid' => $tjiid,
        'data_item_key' => $data_item_key,
      );

      // This is the actual text sent to Lionbridge for translation.
      $translatable_content[$tjiid][$data_item_key] = $value['#text'];
    }
    $file_content = json_encode($translatable_content, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    $file_content = str_replace(array('“', '”'), '\"', $file_content);
    $translation = array(
      'source_language' => $source_language,
      'fileName' => urlencode($file_name . '.json'),
      'content_type' => 'application/json',
      'file_content' => $file_content,
    );

    $file_asset = $this->addFile($translation, $api_client);
    if (isset($file_asset['Errors']['Error'])) {
      throw new TMGMTException($file_asset['Errors']['Error']['DetailedMessage']);
    }
    $file_assets[] = $file_asset['AssetID'];

    /*
     * Create a new project with the file asset, send it to Lionbridge. This
     * will create a new project and return the project XML in the form of an
     * array.
     */
    $project_data = array(
      'project_title' => $project_title,
      'service_id' => $service_id,
      'source_language' => $source_language,
      'target_languages' => $target_languages,
      'product_assets' => $product_assets,
      'file_assets' => $file_assets,
    );
    $project = $this->addProject($project_data, $api_client);
    if (isset($project['Errors']['Error']) && !empty($project['Errors']['Error'])) {
      throw new TMGMTException($project['Errors']['Error']['DetailedMessage']);
    }

    /*
     * Get a quote for the project.
     */
    $quote = $this->generateQuote($project['ProjectID'], $project_complete_url, $api_client);
    if (isset($quote['Errors']) && !empty($quote['Errors'])) {
      throw new TMGMTException($quote['Errors']['Error']['DetailedMessage']);
    }

    // Set the job reference to the Quote ID.
    $job->reference = $quote['QuoteID'];

    /*
     * Add remote mapping data. Now that we have an asset id, project id, and a
     * quote id, add this as remote mapping data to the tmgmt_remote table.
     */
    foreach ($translation_keys as $translation_key) {
      $items[$translation_key['tjiid']]->addRemoteMapping(
        $translation_key['data_item_key'],
        $file_asset['AssetID'],
        array(
          'remote_identifier_2' => $project['ProjectID'],
          'remote_identifier_3' => $quote['QuoteID'],
          'amount' => $quote['TotalCost'],
          'currency' => $quote['Currency'],
        )
      );
    }
  }

  /**
   * Gets status of job items from Lionbridge and downloads translated items.
   *
   * @param TMGMTJob $job
   *   The translation job object.
   */
  public function fetchJob(TMGMTJob $job) {
    $translator = $job->getTranslator();
    $api_client = new TMGMTLionbridgeConnector($translator);
    if ($job->reference) {
      $quote = $api_client->getQuote($job->reference);
      if (!$quote || isset($quote['Error'])) {
        throw new TMGMTException('Could not get quote.');
      }

      $translation_complete = FALSE;

      switch ($quote['Status']) {
        case "Error":
          $job->addMessage('An error occurred with this quote.');
          $this->abortTranslation($job);
          break;

        case "Pending":
          // If this is a pending quote, add a message and a link to approve.
          $job->addMessage(
            'Quote ready for approval: <a href="@url" target="_blank">View on Lionbridge</a>', array(
              '@url' => url($api_client->getEndPoint() . '/project/' . $quote['QuoteID'] . '/details'),
            )
          );
          break;

        case "Authorized":
          $job->addMessage('Translation in progress.');
          break;

        case "Complete":
          /*
           * Get the file which contains translations for all job items.
           * Loop through the remotes, populate translated data.
           */
          $asset_id = $quote['Projects']['Project']['Files']['File']['AssetID'];

          $data_json = $api_client->getFileTranslation($asset_id, $translator->mapToRemoteLanguage($job->target_language));
          $data = json_decode($data_json, TRUE);
          if (!$data) {
            $data_error_message = json_last_error_msg();
            throw new TMGMTException($data_error_message);
          }

          // Batch process the storing of the translated data.
          batch_set(_lionbridge_translation_provider_batch($job, $data));
          $translation_complete = TRUE;
          break;

      }
      return $translation_complete;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(TMGMTJob $job) {
    $translator = $job->getTranslator();
    $api_client = new TMGMTLionbridgeConnector($translator);
    $quote = $api_client->getQuote($job->reference);

    // If the status is Pending reject the quote on Lionbridge.
    if ($quote && empty($quote['Errors']) && $quote['Status'] == 'Pending') {
      $api_client->rejectQuote($quote['QuoteID']);
    }

    $job->aborted();
  }

  /**
   * Sends a file to the Lionbridge translation service.
   */
  private function addFile($translation, TMGMTLionbridgeConnector $api_client) {
    return $api_client->addFile(
      $translation['source_language'],
      $translation['fileName'],
      $translation['content_type'],
      $translation['file_content']
    );
  }

  /**
   * Sends a create project request to the Lionbridge translation service.
   */
  private function addProject($project_data, TMGMTLionbridgeConnector $api_client) {
    return $api_client->addProject(
      $project_data['project_title'],
      $project_data['service_id'],
      $project_data['source_language'],
      $project_data['target_languages'],
      $project_data['product_assets'],
      $project_data['file_assets']
    );
  }

  /**
   * Gets a quote from the Lionbridge translation service.
   */
  private function generateQuote($project_id, $notification_url, TMGMTLionbridgeConnector $api_client) {
    return $api_client->generateQuote($project_id, $notification_url);
  }

}
