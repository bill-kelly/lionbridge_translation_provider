<?php

/**
 * @file
 * Controller class for the Lionbridge translator ui.
 */

/**
 * Lionbridge translator ui controller.
 */
class TMGMTLionbridgeTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    $api_client = new TMGMTLionbridgeConnector($translator);

    if ($translator->isAvailable() == TRUE) {
      $account_info = $api_client->accountInformation();
      $api_version = $api_client->getApiVersion();
      $services = $api_client->listServices();
      $currency = $api_client->getAllowedCurrencies();

      $no_errors = 1;
      if (isset($account_info['head']) && isset($account_info['body'])) {
        drupal_set_message('Invalid <strong>Liondemand Endpoint</strong>.', 'error');
        $no_errors = 0;
      }
      elseif (isset($account_info['Error'])) {
        $err = trim($account_info['Error']);
        $no_errors = 0;

        if ($err === 'Invalid endpoint.') {
          drupal_set_message('Invalid <strong>Liondemand Endpoint</strong>.', 'error');
        }
        elseif ($err === 'Invalid Access Key ID') {
          drupal_set_message('Invalid <strong>Lionbridge Access Key ID</strong>.', 'error');
        }
        elseif (substr($err, 0, 18) === 'Invalid signature.') {
          drupal_set_message('Invalid <strong>Lionbridge Access Key</strong>.', 'error');
        }
      }
    }

    // Check if there is more than one language enabled.
    if (count(language_list()) === 1) {
      drupal_set_message(t('Only one language has been detected, please <a href="@url_add">add new language</a> here.  To see list of languages, <a href="@url_list" target="_blank">click here</a>.', array('@url_add' => url('admin/config/regional/language/add&destination=' . current_path()), '@url_list' => url('admin/config/regional/language'))), 'warning');
    }

    // Check if there is at least one CT with multilingual support enabled.
    if ($api_client->getMultilingualSupportCtCount() === 0) {
      drupal_set_message(t('There must be at least one content type with a multilingual support enabled.'), 'warning');
    }

    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('Liondemand Endpoint'),
      '#default_value' => $translator->getSetting('endpoint'),
      '#description' => t('Please enter your Lionbridge liondemand endpoint url.'),
      '#element_validate' => array('_lionbridge_translation_provider_validate_endpoint_url'),
    );

    $form['access_key_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Lionbridge Access Key Id'),
      '#default_value' => $translator->getSetting('access_key_id'),
      '#description' => t('Please enter your Lionbridge Access Key ID.'),
      '#required' => TRUE,
      '#maxlength' => 20,
    );

    $form['access_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Lionbridge Access Key'),
      '#default_value' => $translator->getSetting('access_key'),
      '#description' => t('Please enter your Lionbridge Access Key.'),
      '#required' => TRUE,
      '#maxlength' => 40,
    );

    $form['po_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Purchase Order Number'),
      '#default_value' => $translator->getSetting('po_number'),
      '#description' => t('Please enter your Lionbridge Purchase Order Number.'),
    );

    // Only show service, currency and account info if translator is available.
    if ($translator->isAvailable() == TRUE && $no_errors == 1) {
      if (is_array($services)) {
        $options = array();
        foreach ($services['Service'] as $service) {
          $options[$service['ServiceID']] = $service['Name'];
        }
        $form['service'] = array(
          '#type' => 'select',
          '#title' => t('Default translation service'),
          '#options' => $options,
          '#default_value' => $translator->getSetting('service'),
        );
      }
      if (is_array($currency)) {
        $form['currency'] = array(
          '#type' => 'select',
          '#title' => t('Default currency'),
          '#options' => drupal_map_assoc($currency),
          '#default_value' => $translator->getSetting('currency'),
        );
      }
      $form['account_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Lionbridge Account Info'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      if (is_array($account_info)) {
        foreach ($account_info as $key => $val) {
          if (is_array($val)) {
            $val = implode('<br>', $val);
          }
          $form['account_info'][$key] = array(
            '#markup' => '<strong>' . $key . ':</strong> ' . $val . '<br>',
          );
        }
      }
      $form['account_info']['api_version'] = array(
        '#type' => 'item',
        '#title' => t('Api Version'),
      );
      $form['account_info']['api_version']['version_id'] = array(
        '#markup' => $api_version,
      );
    }

    return parent::pluginSettingsForm($form, $form_state, $translator);
  }

  /**
   * Overrides TMGMTDefaultTranslatorUIController::checkoutSettingsForm().
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    $settings['project_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Project title'),
      '#size' => 60,
      '#default_value' => $job->label(),
    );

    return $settings;
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutInfo().
   */
  public function checkoutInfo(TMGMTJob $job) {
    $form = array();
    $translator = $job->getTranslator();
    $api_client = new TMGMTLionbridgeConnector($translator);
    $quote = $api_client->getQuote($job->reference);

    $form['quote'] = array(
      '#type' => 'item',
      '#title' => t('Quote @quote_id', array('@quote_id' => $quote['QuoteID'])),
    );
    $form['quote']['cost'] = array(
      '#type' => 'item',
      '#markup' => t('<strong>Total Cost:</strong> @cost @currency', array(
        '@cost' => $quote['TotalCost'],
        '@currency' => $quote['Currency'],
      )
      ),
    );
    $form['quote']['status'] = array(
      '#type' => 'item',
      '#markup' => t('<strong>Status:</strong> @status', array(
        '@status' => $quote['Status'],
      )
      ),
    );

    if ($quote['Status'] == 'Pending') {
      $form['actions']['authorize'] = array(
        '#type' => 'submit',
        '#value' => t('Authorize Quote'),
        '#submit' => array('_lionbridge_translation_provider_authorize_quote'),
        '#weight' => -10,
      );
    }

    if ($job->isActive()) {
      $form['actions']['poll'] = array(
        '#type' => 'submit',
        '#value' => t('Poll translations'),
        '#submit' => array('_lionbridge_translation_provider_poll_submit'),
        '#weight' => -10,
      );
    }

    return $form;
  }

}
