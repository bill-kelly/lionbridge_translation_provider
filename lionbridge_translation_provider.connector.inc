<?php
/**
 * @file
 * Connector class for Lionbridge onDemand API service.
 */

/**
 * Connects Lionbridge API to TMGMT.
 */
class TMGMTLionbridgeConnector {
  /**
   * Purchase Order Number.
   */
  const PURCHASE_ORDER_NUMBER = '123456';

  /**
   * API Key ID.
   *
   * @var string
   */
  private $keyId;

  /**
   * API Secret ID.
   *
   * @var string
   */
  private $secretId;

  /**
   * API url.
   *
   * @var string
   */
  private $endPoint;

  /**
   * Active currency.
   *
   * @var string
   */
  private $currency;

  /**
   * Api version - Check current version here http://api-docs.liondemand.com/.
   *
   * @var string
   */
  private $apiVersion = '2015-02-23';

  /**
   * List of allowed currencies.
   *
   * @var array
   */
  private $allowedCurrencies = array('USD', 'EUR', 'GBP');

  /**
   * Optional Purchase Order Number.
   *
   * @var string
   */
  private $poNumber;

  /**
   * Construct class and set default params.
   *
   * @param \TMGMTTranslator $translator
   *   Constructs the connector.
   */
  public function __construct(TMGMTTranslator $translator) {
    $this->keyId = $translator->getSetting('access_key_id');
    $this->secretId = $translator->getSetting('access_key');
    $this->currency = $translator->getSetting('currency');
    $this->endPoint = $translator->getSetting('endpoint');
    $this->poNumber = $translator->getSetting('po_number');
  }

  /**
   * Return xml with account information.
   *
   * @return string
   *   Gets Lionbridge account info.
   */
  public function accountInformation() {
    return $this->getResource('/api/account/info');
  }

  /**
   * Gets the currency property.
   *
   * @return string
   *   The currency (USD, EUR, GBP).
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * Sets the currency property.
   *
   * @param string $currency
   *   The currency (USD, EUR, GBP).
   */
  public function setCurrency($currency) {
    $allowed_currencies = $this->getAllowedCurrencies();
    $this->currency = in_array($currency, $allowed_currencies) ? $currency : current($allowed_currencies);
  }

  /**
   * Gets the endpoint.
   *
   * @return string
   *   The Lionbridge API endpoint.
   */
  public function getEndPoint() {
    return $this->endPoint;
  }

  /**
   * Sets the endpoint.
   *
   * @param string $end_point
   *   The Lionbridge API endpoint.
   */
  public function setEndPoint($end_point) {
    $this->endPoint = $end_point;
  }

  /**
   * Gets the API key ID.
   *
   * @return string
   *   The API key ID.
   */
  public function getKeyId() {
    return $this->keyId;
  }

  /**
   * Sets the API key ID.
   *
   * @param string $key_id
   *   The API key ID.
   */
  public function setKeyId($key_id) {
    $this->keyId = $key_id;
  }

  /**
   * Gets the API key secret ID.
   *
   * @return string
   *   The API key secret ID.
   */
  public function getSecretId() {
    return $this->secretId;
  }

  /**
   * Sets the API key secret ID.
   *
   * @param string $secret_id
   *   The API key secret ID.
   */
  public function setSecretId($secret_id) {
    $this->secretId = $secret_id;
  }

  /**
   * Gets the API version.
   *
   * @return string
   *   The API version.
   */
  public function getApiVersion() {
    return $this->apiVersion;
  }

  /**
   * Sets the API version.
   *
   * @param string $api_version
   *   The API version.
   */
  public function setApiVersion($api_version) {
    $this->apiVersion = $api_version;
  }

  /**
   * Gets the list of allowed currencies.
   *
   * @return array
   *   A list of currencies.
   */
  public function getAllowedCurrencies() {
    return $this->allowedCurrencies;
  }

  /**
   * Sets the allowed currencies.
   *
   * @param array $allowed_currencies
   *   An array of currency codes.
   */
  public function setAllowedCurrencies(array $allowed_currencies) {
    $this->allowedCurrencies = $allowed_currencies;
  }

  /**
   * Return xml with services details.
   *
   * @return object
   *   Return xml with services details.
   */
  public function listServices() {
    return $this->getResource('/api/services');
  }

  /**
   * Return xml with service details.
   *
   * @return object
   *   Return xml with service details.
   */
  public function getService($service_id) {
    return $this->getResource('/api/services/' . $service_id);
  }

  /**
   * Returns xml with lis of locales.
   *
   * @return object
   *   Returns xml with lis of locales.
   */
  public function listLocales() {
    return $this->getResource('/api/locales');
  }

  /**
   * Return xml list of all files.
   *
   * @return object
   *   Return xml list of all files.
   */
  public function listFiles() {
    return $this->getResource('/api/files');
  }

  /**
   * Return xml with details for a single project.
   *
   * @param string $project_id
   *   The project id returned from Lionbridge. Stored as remote_identifier_2.
   *
   * @return object
   *   Return xml with details for a single project.
   */
  public function getProject($project_id) {
    return $this->getResource('/api/projects/' . $project_id);
  }

  /**
   * Return xml with details for a single project.
   *
   * @param string $quote_id
   *   The quote id returned from Lionbridge. Stored as remote_identifier_3.
   *
   * @return object
   *   Return xml with details for a single quote.
   */
  public function getQuote($quote_id) {
    return $this->getResource('/api/quote/' . $quote_id);
  }

  /**
   * Authorize quote for a given job.
   *
   * @param array $quote
   *   The quote returned from getQuote().
   *
   * @return object
   *   Return xml with success or error.
   */
  public function authorizeQuote(array $quote) {
    $projects_xml = format_xml_elements($quote['Projects']);
    $count_files = count($quote['Projects']['Project']['Files']['File']);

    if ($this->poNumber === '') {
      $po_number = self::PURCHASE_ORDER_NUMBER;
    }
    else {
      $po_number = $this->poNumber;
    }

    $quote_xml = <<<XML
<Quote>
    <QuoteID>{$quote['QuoteID']}</QuoteID>
    <CreationDate>{$quote['CreationDate']}</CreationDate>
    <ServiceID>{$quote['Projects']['Project']['ServiceID']}</ServiceID>
    <SourceLanguage>
        <LanguageCode>{$quote['Projects']['Project']['SourceLanguage']['LanguageCode']}</LanguageCode>
    </SourceLanguage>
    <TargetLanguages>
        <TargetLanguage>
            <LanguageCode>{$quote['Projects']['Project']['TargetLanguages']['TargetLanguage']['LanguageCode']}</LanguageCode>
        </TargetLanguage>
    </TargetLanguages>
    <TotalTranslations>{$count_files}</TotalTranslations>
    <TranslationCredit>0</TranslationCredit>
    <TotalCost>{$quote['TotalCost']}</TotalCost>
    <PrepaidCredit>{$quote['PrepaidCredit']}</PrepaidCredit>
    <PurchaseOrderNumber>{$po_number}</PurchaseOrderNumber>
    <AmountDue>{$quote['AmountDue']}</AmountDue>
    <Projects>{$projects_xml}</Projects>
    <Currency>{$quote['Currency']}</Currency>
</Quote>
XML;
    $resource = $this->sendResource(
      "/api/quote/{$quote['QuoteID']}/authorize",
      $quote_xml,
      array(
        'Content-type: text/xml',
      )
    );
    return $resource;
  }

  /**
   * Rejects a quote from Lionbridge.
   *
   * @param string $quote_id
   *   The quote id returned from Lionbridge. Stored as remote_identifier_3.
   *
   * @return object
   *   Return xml with details for a single quote.
   */
  public function rejectQuote($quote_id) {
    return $this->sendResource('/api/quote/' . $quote_id . '/reject');
  }

  /**
   * Return xml with details for a single file.
   *
   * @param string $asset_id
   *   The asset id returned from Lionbridge. Stored as remote_identifier_1.
   *
   * @return object
   *   Return xml with details for a single file.
   */
  public function getFileDetails($asset_id) {
    return $this->getResource('/api/files/' . $asset_id . '/details');
  }

  /**
   * Returns the content of a translated file.
   *
   * @param string $asset_id
   *   The asset id returned from Lionbridge. Stored as remote_identifier_1.
   * @param string $lang_code
   *   The language code of the desired translation. Ex: 'fr-fr'.
   *
   * @return object
   *   The translated text.
   */
  public function getFileTranslation($asset_id, $lang_code) {
    return $this->getResource('/api/files/' . $asset_id . '/' . $lang_code);
  }

  /**
   * Upload file for future translation.
   *
   * @param string $language_code
   *   The language code of the source text. Ex: 'fr-fr'.
   * @param string $file_name
   *   The name of the file.
   * @param string $content_type
   *   The content type of the file.
   * @param string $file_content
   *   The file contents.
   *
   * @return array
   *   The file information from Lionbridge, including the AssetID.
   */
  public function addFile($language_code, $file_name, $content_type, $file_content) {
    $resource = '/api/files/add/' . $language_code . '/' . $file_name;
    return $this->sendResource($resource, $file_content, array(
      'Content-type:' . $content_type,
    ));
  }

  /**
   * Creates a project from previously uploaded files.
   *
   * @param string $project_title
   *   The title of the project from the checkout settings form.
   * @param string $service_id
   *   The ID of the service chosen on the checkout settings form.
   * @param string $source_language
   *   The language code of the source data in the form of 'en-us'.
   * @param array $target_languages
   *   The language code of the desired translation in the form of 'fr-fr'.
   * @param array $products
   *   An array of product IDs for a product-based translation.
   * @param array $files
   *   An array of asset IDs for a file-based translation.
   *
   * @return mixed
   *   The project data from Lionbridge.
   */
  public function addProject($project_title, $service_id, $source_language, array $target_languages, $products = array(), $files = array()) {
    $target_languages_xml = $products_xml = array();
    foreach ($target_languages as $target_lang) {
      $target_languages_xml[] = <<<TRGT_LNG
<TargetLanguage>
    <LanguageCode>{$target_lang}</LanguageCode>
</TargetLanguage>
TRGT_LNG;
    }
    $target_languages_xml = implode(PHP_EOL, $target_languages_xml);

    foreach ($products as $product) {
      $products_xml[] = $product->toXml();
    }
    $products_xml = "<Products>" . implode(PHP_EOL, $products_xml) . "</Products>";

    foreach ($files as &$file) {
      $file = "<File><AssetID>{$file}</AssetID></File>";
    }
    $files_xml = "<Files>" . implode(PHP_EOL, $files) . "</Files>";

    $project_xml = <<<PROJECT_XML
<AddProject>
    <ProjectName>{$project_title}</ProjectName>
    <TranslationOptions>
        <ServiceID>{$service_id}</ServiceID>
        <SourceLanguage>
            <LanguageCode>{$source_language}</LanguageCode>
        </SourceLanguage>
        <TargetLanguages>
            {$target_languages_xml}
         </TargetLanguages>
    </TranslationOptions>
    {$products_xml}
    {$files_xml}
</AddProject>
PROJECT_XML;

    $result = $this->sendResource(
      '/api/projects/add',
      $project_xml,
      array(
        'Content-type: text/xml',
      )
    );
    return $result;

  }

  /**
   * Generates a quote for uploaded files.
   *
   * @param string $project_id
   *   The  project id returned from addProject().
   * @param string $notification_url
   *   The URL to which Lionbirdge will POST a project complete notification.
   *
   * @return mixed
   *   The quote data from Lionbridge.
   */
  public function generateQuote($project_id, $notification_url) {
    $quote_xml = <<<QUOTE_XML
<GenerateQuote>
    <TranslationOptions>
        <Currency>{$this->getCurrency()}</Currency>
        <NotifyCompleteURL>{$notification_url}</NotifyCompleteURL>
    </TranslationOptions>
    <Projects>
        <Project>
            <ProjectID>{$project_id}</ProjectID>
        </Project>
    </Projects>
</GenerateQuote>
QUOTE_XML;

    $result = $this->sendResource(
      '/api/quote/generate',
      $quote_xml,
      array(
        'Content-type: text/xml',
      )
    );
    return $result;
  }

  /**
   * Sends a GET request to Lionbridge services.
   *
   * @param string $resource
   *   The path to send the request.
   * @param string $method
   *   The request method.
   *
   * @return mixed
   *   Either text if it's a translation or and array with requested data.
   */
  protected function getResource($resource, $method = 'GET') {
    $options = array(
      'headers' => $this->generateAuthHeaders($resource),
      'method' => $method,
    );

    $url = $this->getEndPoint() . $resource;

    $response = drupal_http_request($url, $options);
    $content_type = $response->headers['content-type'];
    switch ($content_type) {
      case 'text/plain':
      case 'application/json':
        $result = $response->data;
        break;

      default:
        $xml_data = $response->data;
        @$xml_obj = simplexml_load_string($xml_data);
        if (!$xml_obj) {
          /* throw new \Exception('Invalid xml', $response->code); */
          $result = array(
            'Error' => 'Invalid endpoint.',
          );
        }
        else {
          $result_data = $this->xmlToArray($xml_obj);
          if ($result_data) {
            $result = $result_data;
          }
        }
        break;

    }

    return $result;
  }

  /**
   * Sends a POST request to Lionbridge.
   *
   * @param string $resource
   *   The path to send the request.
   * @param string $data
   *   Data to be sent to Lionbridge.
   * @param array $additional_headers
   *   Any additional request headers to be sent.
   *
   * @return mixed
   *   The XML data returned from Lionbridge converted to an array.
   */
  protected function sendResource($resource, $data = '', $additional_headers = array()) {
    $auth_headers = $this->generateAuthHeaders($resource, 'POST');

    // Concatenate keys and values of auth array.
    foreach ($auth_headers as $key => $value) {
      $headers[] = $key . ': ' . $value;
    }

    if (!empty($headers)) {
      $request_headers = array_merge($headers, $additional_headers);
    }

    $url = $this->getEndPoint() . $resource;

    $url = url($url,
      array(
        'absolute' => TRUE,
        'external' => TRUE,
        'https' => TRUE,
      )
    );

    $request = curl_init();
    curl_setopt($request, CURLOPT_URL, $url);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($request, CURLOPT_POST, 1);
    curl_setopt($request, CURLOPT_POSTFIELDS, $data);
    curl_setopt($request, CURLOPT_HTTPHEADER, $request_headers);
    $response = curl_exec($request);
    curl_close($request);

    @$xml_obj = simplexml_load_string($response);
    $result_data = $this->xmlToArray($xml_obj);

    return $result_data;
  }

  /**
   * Converts XML to an array.
   *
   * @param \SimpleXMLElement $xml_object
   *   The XML returned from Lionbridge.
   *
   * @return mixed
   *   An array of data converted from the XML object.
   */
  private function xmlToArray(\SimpleXMLElement $xml_object) {
    return json_decode(json_encode((array) $xml_object), TRUE);
  }

  /**
   * Generates the authorization header required by Lionbridge.
   *
   * @param string $resource
   *   The path to which the request will be sent.
   *
   * @return array
   *   An array of authorization headers.
   */
  public function generateAuthHeaders($resource, $method = 'GET', $micro_time = NULL) {
    date_default_timezone_set('America/New_York');
    if (is_null($micro_time)) {
      $micro_time = microtime(TRUE);
    }
    $micro = sprintf("%06d", ($micro_time - floor($micro_time)) * 1000000);
    $d = new \DateTime(date('Y-m-d H:i:s.' . $micro, $micro_time));
    $date_time = $d->format('Y-m-d\TH:i:s.u');

    $signature = "{$method}:{$resource}:{$this->getSecretId()}:{$date_time}:{$this->getApiVersion()}:text/xml";
    $signature = base64_encode(hash('sha256', $signature, TRUE));
    $authorization = "LOD1-BASE64-SHA256 KeyID={$this->getKeyId()},Signature={$signature},SignedHeaders=x-lod-timestamp;x-lod-version;accept";

    return array(
      "Accept" => "text/xml",
      "Authorization" => $authorization,
      "x-lod-version" => $this->getApiVersion(),
      "x-lod-timestamp" => $date_time,
    );
  }

  /**
   * Count all content types supporting multilingual support.
   */
  public function getMultilingualSupportCtCount() {
    $cts = node_type_get_types();
    $i = 0;
    foreach ($cts as $ct) {
      if (variable_get('language_content_type_' . $ct->type) > 0) {
        $i++;
      }
    }

    return $i;
  }

}
